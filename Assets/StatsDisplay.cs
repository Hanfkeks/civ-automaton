﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class StatsDisplay : MonoBehaviour
    {

        #region Properties and Fields
        public GameObject Container;
        private bool containerEnabled = true;

        public Text PlayerText;
        public Image ButtonNext;
        public Image ButtonPrev;
        public Text Population;
        public Text AvgStrength;
        public Text TechLevel;
        public Text TechProgress;
        public Text CellsControlled;
        public Text InfectedPopulation;

        public int FactionNr = 0;
        public Faction Faction
        {
            get
            {
                return Automaton.Factions[FactionNr];
            }
        }

        #endregion Properties and Fields

        #region Methods

        public void Start()
        {
            NextFaction();
        }

        public void Update()
        {
            if (Faction.Population <= 0)
            {
                NextFaction();
            }

            PlayerText.text = "Player " + FactionNr;
            Population.text = Faction.Population.ToString();
            AvgStrength.text = (Faction.TotalStrength / Faction.Population).ToString("0.0");
            TechLevel.text = Faction.TechString;
            TechProgress.text = ((Faction.Science/Faction.NextTechCost)*100).ToString("0.00")+"%";
            CellsControlled.text = Faction.ControlledFields.ToString();
            InfectedPopulation.text = (Faction.InfectedPopulation / ((float)Faction.Population) * 100).ToString("0.00") + "%";
            ButtonNext.color = FactionRenderer.FactionColors[FactionNr];
            ButtonPrev.color = FactionRenderer.FactionColors[FactionNr];


            CheckRestart();
        }

        public void ToggleEnabled()
        {
            containerEnabled = !containerEnabled;
            Container.SetActive(containerEnabled);
        }

        public void CheckRestart()
        {
            if (Input.GetKeyDown(KeyCode.N))
            {
                Automaton.RequestRestart = true;
            }
            int factionsIngame = 0;
            foreach (var faction in Automaton.Factions)
            {
                if (faction.Population > 0)
                {
                    factionsIngame++;
                }
            }
            if (factionsIngame == 1)
            {
                Automaton.RequestRestart = true;
            }
        }


        public void NextFaction()
        {
            if (Automaton.Factions != null)
            {
                FactionNr = (FactionNr + 1) % Automaton.Factions.Length;
                int tries = 0;
                while (Faction.Population == 0 && tries < Automaton.Factions.Length)
                {

                    FactionNr = (FactionNr + 1) % Automaton.Factions.Length;
                    tries++;
                }
            }
        }

        public void PrevFaction()
        {
            FactionNr = (FactionNr - 1 + Automaton.Factions.Length) % Automaton.Factions.Length;
            int tries = 0;
            while (Faction.Population == 0 && tries < Automaton.Factions.Length)
            {

                FactionNr = (FactionNr - 1 + Automaton.Factions.Length) % Automaton.Factions.Length;
                tries++;
            }

        }

        #endregion Methods
    }
}
