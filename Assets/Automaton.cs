﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets;
using UnityEngine.UI;
using System.Threading;
using System;

public class Automaton : MonoBehaviour
{

    public Sprite LoadedMap;
    public Image DisplayMap;
    public static PixelRenderer PixelRenderer = new FactionRenderer();
    public static Faction[] Factions;
    public StatsDisplay StatsDisplay;
    public Color[] Colors;
    public bool Paused;

    public static bool RequestRestart = false;



    public Vector2 SwipeStartPosition;
    public float SwipeStartTime;
    public int lastMode = 0;

    public List<CoreTask> Cores = new List<CoreTask>();

    public static uint UpdateID = 0;

    public static Cell[,] Cells;

    public Texture2D Display;
    public volatile bool ReadyForNext = false;
    public static List<Explosion> QueuedExplosions = new List<Explosion>();

    public static long PeopleUpdates = 0;

    private bool switchedMode = false;

    [ThreadStatic]
    static System.Random _gameRand = new System.Random();

    public static System.Random GameRand
    {
        get
        {
            if (_gameRand == null)
            {
                _gameRand = new System.Random();
            }
            return _gameRand;
        }
    }

    // Use this for initialization
    void Start()
    {
        Init();
        
    }

    public void Init()
    {
        LoadMap();

        Debug.Log("Loaded Map");
        Display = new Texture2D(Cells.GetLength(0), Cells.GetLength(1));
        DisplayMap.overrideSprite = Sprite.Create(Display, DisplayMap.sprite.rect, DisplayMap.sprite.pivot);
        //Display = DisplayMap.sprite.texture;
        int factioncount = Colors.Length;
        FactionRenderer.FactionColors = Colors;
        int i = 0;
        Color[] colarr = new Color[factioncount + 1];
        while (i <= factioncount)
        {
            if (i < FactionRenderer.FactionColors.Length)
            {
                colarr[i] = FactionRenderer.FactionColors[i];
            }
            else
            {
                colarr[i] = new Color((float)GameRand.NextDouble(), (float)GameRand.NextDouble(), (float)GameRand.NextDouble());
            }
            i++;
        }
        FactionRenderer.FactionColors = colarr;
        SpawnFactions(factioncount);
        QuadCoreTasks();
        Debug.Log("Spawned all Factions, lets go");
        ReadyForNext = true;
        StatsDisplay.NextFaction();
    }

    // Update is called once per frame
    void Update()
    {
        CheckRestart();
        CheckInputs();
        if (!ReadyForNext)
        {
            return;
        }
        if (Paused)
        {
            return;
        }
        PeopleUpdates = 0;
        UpdateID++;

        foreach (Faction fac in Factions)
        {
            fac.ResetStats();
        }

        //Debug.Log("Frame "+UpdateID);
        //Update cells
        foreach (CoreTask core in Cores)
        {
            core.Prepare();
            core.StartWorking();
        }
        int x;
        int y;
        ReadyForNext = false;
        foreach (CoreTask core in Cores)
        {
            core.Worker.Join(10000);
            //Worker has finished
            //Now update the borders

            x = core.MinX;
            y = core.MinY;
            while (y <= core.MaxY)
            {
                Cells[x, y].Update();

                y++;
            }

            x = core.MaxX;
            y = core.MinY;
            while (y <= core.MaxY)
            {
                Cells[x, y].Update();

                y++;
            }

            x = core.MinX + 1;
            y = core.MinY;
            while (x < core.MaxX)
            {
                Cells[x, y].Update();

                x++;
            }

            x = core.MinX + 1;
            y = core.MaxY;
            while (x < core.MaxX)
            {
                Cells[x, y].Update();

                x++;
            }

        }
        // Calculate Explosions
        foreach (Explosion ex in QueuedExplosions)
        {
            ex.Explode();
        }
        QueuedExplosions.Clear();

        ReadyForNext = true;
        x = 0;
        y = 0;
        while (x < LoadedMap.texture.width)
        {
            while (y < LoadedMap.texture.height)
            {
                Display.SetPixel(x, y, PixelRenderer.RenderPixel(Cells[x, y]));
                y++;
            }
            y = 0;
            x++;
        }

        Display.Apply();

    }

    public void CheckRestart()
    {
        if (RequestRestart)
        {
            RequestRestart = false;
            Init();
        }
    }

    public void CheckInputs()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Paused = !Paused;
        }
        foreach (Faction f in Factions)
        {
            //f.DebugFaction();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            lastMode = 0;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            lastMode = 1;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            lastMode = 2;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            lastMode = 3;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            lastMode = 4;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            lastMode = 5;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            lastMode = 6;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            lastMode = 7;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            lastMode = 8;
            switchedMode = true;
        }
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            lastMode = 9;
            switchedMode = true;
        }

        if (PixelRenderer is StrengthRenderer)
        {
            Debug.Log("Maximum Strength = " + ((StrengthRenderer)PixelRenderer).LastMaxStrength);
        }

        //Swipes
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Vector2 endPosition = Input.GetTouch(0).position;
            Vector2 delta = endPosition - SwipeStartPosition;

            float dist = Mathf.Sqrt(Mathf.Pow(delta.x, 2) + Mathf.Pow(delta.y, 2));
            float angle = Mathf.Atan(delta.y / delta.x) * (180.0f / Mathf.PI);
            float duration = Time.time - SwipeStartTime;
            float speed = dist / duration;

            // Left to right swipe
            if (SwipeStartPosition.y < endPosition.y)
            {
                if (angle < 0) angle = angle * -1.0f;

                if (dist > 300 && angle < 10 && speed > 1000)
                {
                    switchedMode = true;
                    lastMode++;
                }
            }
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            SwipeStartPosition = Input.GetTouch(0).position;
            SwipeStartTime = Time.time;
        }

        lastMode = lastMode % 10;

        if (switchedMode)
        {
            switchedMode = false;
            switch (lastMode)
            {
                case 0:
                    PixelRenderer = new FactionRenderer();
                    break;
                case 1:
                    PixelRenderer = new HabitabilityRenderer();
                    break;
                case 2:
                    PixelRenderer = new UpgradeRenderer();
                    break;
                case 3:
                    PixelRenderer = new PopulationRenderer();
                    break;
                case 4:
                    PixelRenderer = new StrengthRenderer();
                    break;
                case 5:
                    PixelRenderer = new DoctorRenderer();
                    break;
                case 6:
                    PixelRenderer = new FalloutRenderer();
                    break;
                case 7:
                    PixelRenderer = new ScienceRenderer();
                    break;
                case 8:
                    PixelRenderer = new SuperstructureRenderer();
                    break;
                case 9:
                    PixelRenderer = new LifeExpectancyRenderer();
                    break;
            }
        }


        if (PixelRenderer is PopulationRenderer)
        {
            Debug.Log("TotalPop = " + PeopleUpdates);
        }
    }

    public void QuadCoreTasks()
    {
        Cores.Clear();
        int xborder = Cells.GetLength(0) / 2;
        int yborder = Cells.GetLength(1) / 2;
        Cores.Add(new CoreTask(0, xborder, 0, yborder, Display));
        Cores.Add(new CoreTask(xborder + 1, Cells.GetLength(0) - 1, 0, yborder, Display));
        Cores.Add(new CoreTask(0, xborder, yborder + 1, Cells.GetLength(1) - 1, Display));
        Cores.Add(new CoreTask(xborder + 1, Cells.GetLength(0) - 1, yborder + 1, Cells.GetLength(1) - 1, Display));
    }

    public static void QueueExplosion(int x, int y, int range, ExplosionType type, int faction = 0)
    {
        lock (QueuedExplosions)
        {
            QueuedExplosions.Add(new Explosion(x, y, range, type, faction));
        }
    }

    public void SpawnFactions(int maxFactions)
    {
        int curFaction = 1;
        Factions = new Faction[maxFactions + 1];
        Factions[0] = new Faction(0);
        while (maxFactions >= curFaction)
        {
            Factions[curFaction] = new Faction(curFaction);
            bool spawnPosFound = false;
            int rx = 0;
            int ry = 0;
            while (!spawnPosFound)
            {
                rx = UnityEngine.Random.Range(0, Cells.GetLength(0));
                ry = UnityEngine.Random.Range(0, Cells.GetLength(1));
                spawnPosFound = true;
                spawnPosFound &= (IsLand(rx, ry));
                spawnPosFound &= (IsLand(rx, ry + 1));
                spawnPosFound &= (IsLand(rx, ry - 1));

                spawnPosFound &= (IsLand(rx + 1, ry));
                spawnPosFound &= (IsLand(rx + 1, ry + 1));
                spawnPosFound &= (IsLand(rx + 1, ry - 1));

                spawnPosFound &= (IsLand(rx - 1, ry));
                spawnPosFound &= (IsLand(rx - 1, ry + 1));
                spawnPosFound &= (IsLand(rx - 1, ry - 1));
                Debug.Log("Rerolling Spawn position for Faction " + curFaction);
            }

            new Person(rx, ry, curFaction, 20.0, true);

            new Person(rx + 1, ry + 1, curFaction, 20.0, true);
            new Person(rx + 1, ry - 1, curFaction, 20.0, true);
            new Person(rx - 1, ry + 1, curFaction, 20.0, true);
            new Person(rx - 1, ry - 1, curFaction, 20.0, true);

            new Person(rx, ry + 1, curFaction, 20.0, false);
            new Person(rx, ry - 1, curFaction, 20.0, false);
            new Person(rx - 1, ry, curFaction, 20.0, false);
            new Person(rx - 1, ry, curFaction, 20.0, false);

            new Person(rx, ry, curFaction, 20.0, false);

            new Person(rx + 1, ry + 1, curFaction, 20.0, false);
            new Person(rx + 1, ry - 1, curFaction, 20.0, false);
            new Person(rx - 1, ry + 1, curFaction, 20.0, false);
            new Person(rx - 1, ry - 1, curFaction, 20.0, false);

            new Person(rx, ry + 1, curFaction, 20.0, true);
            new Person(rx, ry - 1, curFaction, 20.0, true);
            new Person(rx - 1, ry, curFaction, 20.0, true);
            new Person(rx - 1, ry, curFaction, 20.0, true);

            Debug.Log("Spawned Faction " + curFaction);

            curFaction++;
        }
    }

    public bool InGrid(int x, int y)
    {
        return Cells.GetLength(0) > x && x > 0 && y > 0 && Cells.GetLength(1) > y;
    }

    public bool IsLand(int x, int y)
    {
        if (!InGrid(x, y))
        {
            return false;
        }
        if (Cells[x, y].Water)
        {
            return false;
        }
        return true;
    }

    public static Cell GetPropperCell(int x, int y)
    {
        while (x < 0)
        {
            x += Cells.GetLength(0);
        }
        if (x > Cells.GetLength(0))
        {
            x = x % Cells.GetLength(0);
        }
        if (Cells.GetLength(0) > x && x >= 0 && y >= 0 && Cells.GetLength(1) > y)
        {
            return Cells[x, y];
        }
        return null;
    }

    public static bool Infect(int x, int y, double intensity, bool areaInfect = false)
    {
        double meanness = (intensity * ((GameRand.NextDouble() * 1.0) + 0.75));

        if (Cells.GetLength(0) > x && x >= 0 && y >= 0 && Cells.GetLength(1) > y)
        {
            if (Cells[x, y].PeopleCount == 0)
            {
                return false;
            }
            else
            {
                if (areaInfect)
                {
                    foreach (Person p in Cells[x, y].People)
                    {
                        p.Infected = true;
                        p.Immune = false;
                        p.InfectionSeverity = Math.Max(p.InfectionSeverity, meanness);
                    }
                }
                else
                {
                    Person p = Cells[x, y].AnyPerson;
                    p.Infected = true;
                    p.InfectionSeverity = Math.Max(p.InfectionSeverity, meanness);
                }
            }
            return true;
        }
        return false;
    }

    public static bool Cure(int x, int y, bool immunize = true)
    {
        if (Cells.GetLength(0) > x && x >= 0 && y >= 0 && Cells.GetLength(1) > y)
        {
            if (Cells[x, y].PeopleCount == 0)
            {
                return false;
            }
            else
            {
                foreach (Person per in Cells[x, y].People)
                {
                    if (immunize)
                    {
                        per.Immune = true;
                    }
                    else
                    {
                        per.Infected = false;
                    }
                }
                foreach (Person per in Cells[x, y].JoiningPeople)
                {
                    if (immunize)
                    {
                        per.Immune = true;
                    }
                    else
                    {
                        per.Infected = false;
                    }
                }
            }
            return true;
        }
        return false;
    }

    public static int CanMove(Person p, int x, int y)
    {
        if (Cells.GetLength(0) > x && x >= 0 && y >= 0 && Cells.GetLength(1) > y)
        {
            return Cells[x, y].CanEnter(p);
        }
        return 0;
    }

    public void LoadMap()
    {
        Cells = new Cell[LoadedMap.texture.width, LoadedMap.texture.height];
        int x = 0;
        int y = 0;
        while (x < LoadedMap.texture.width)
        {
            while (y < LoadedMap.texture.height)
            {
                var pix = LoadedMap.texture.GetPixel(x, y);
                float r = pix.r;
                float g = pix.g;
                float b = pix.b;
                Cells[x, y] = new Cell(x, y, r, g, b);
                y++;
            }
            y = 0;
            x++;
        }
    }

    public void ButtonClick_Factions()
    {
        lastMode = 0;
        switchedMode = true;
    }

    public void ButtonClick_Habitat()
    {
        lastMode = 1;
        switchedMode = true;
    }

    public void ButtonClick_Improvements()
    {
        lastMode = 2;
        switchedMode = true;
    }

    public void ButtonClick_Population()
    {
        lastMode = 3;
        switchedMode = true;
    }

    public void ButtonClick_Strength()
    {
        lastMode = 4;
        switchedMode = true;
    }

    public void ButtonClick_Plague()
    {
        lastMode = 5;
        switchedMode = true;
    }

    public void ButtonClick_Structures()
    {
        lastMode = 8;
        switchedMode = true;
    }

    public void ButtonClick_Fallout()
    {
        lastMode = 6;
        switchedMode = true;
    }

}
