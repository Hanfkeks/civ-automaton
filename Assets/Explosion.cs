﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class Explosion
    {
        public int X;
        public int Y;
        public int Range;
        public ExplosionType Type;
        public int Faction;

        public Explosion(int x, int y, int range, ExplosionType type = ExplosionType.Nuclear, int sender = 0)
        {
            X = x;
            Y = y;
            Range = range - 1;
            Type = type;
            Faction = sender;
        }

        public void Explode()
        {
            int cx = X - Range;
            int cy = Y - Range;
            Vector2 center = new Vector2(X, Y);
            bool rectangular = false;
            if (Type == ExplosionType.RectangularLaser)
            {
                rectangular = true;
            }
            while (cx <= X + Range)
            {
                cy = Y - Range;
                while (cy <= Y + Range)
                {
                    if (rectangular || Vector2.Distance(center, new Vector2(cx, cy)) < Range)
                    {
                        Cell c = Automaton.GetPropperCell(cx, cy);
                        if (c != null)
                        {
                            switch (Type)
                            {
                                case ExplosionType.Nuclear:
                                    c.Annihilate();
                                    c.Fallout += (int)((Range - Vector2.Distance(center, new Vector2(cx, cy))) * Range);
                                    c.NeedsUpdate = true;
                                    break;
                                case ExplosionType.Heal:
                                    Automaton.Cure(c.X, c.Y, true);
                                    break;
                                case ExplosionType.Bio:
                                    Automaton.Infect(c.X, c.Y, Range * 3, true);
                                    break;
                                case ExplosionType.Conversion:
                                    if (c.PeopleCount > 0)
                                    {
                                        foreach (Person p in c.People)
                                        {
                                            p.Faction = Faction;
                                        }
                                        foreach (Person p in c.JoiningPeople)
                                        {
                                            p.Faction = Faction;
                                        }
                                        c.Faction = Faction;
                                    }
                                    break;
                                case ExplosionType.RectangularLaser:
                                case ExplosionType.Laser:
                                    if (c.Faction != this.Faction && c.Faction != 0)
                                    {
                                        c.Annihilate();
                                        c.NeedsUpdate = true;
                                        //Debug.Log("Laser Destruction!");

                                    }

                                    break;
                                case ExplosionType.Terraform:
                                    c.Water = false;
                                    c.NaturalHabitability = Math.Max(c.NaturalHabitability, 3);
                                    c.NaturalFertility = Math.Max(c.NaturalFertility, 3);
                                    break;
                                case ExplosionType.StrengthDuplication:
                                    if (c.Faction == Faction)
                                    {
                                        foreach (var person in c.JoiningPeople)
                                        {
                                            person.Strength *= 2;
                                        }
                                        foreach (var person in c.People)
                                        {
                                            person.Strength *= 2;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    cy++;
                }
                cx++;
            }
        }

    }

    public enum ExplosionType
    {
        Nuclear,
        Heal,
        Bio,
        Conversion,
        Laser,
        RectangularLaser,
        Terraform,
        StrengthDuplication,
    }
}
