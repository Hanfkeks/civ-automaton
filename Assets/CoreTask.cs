﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Assets
{
    public class CoreTask
    {
        //All exclusive!!!
        public int MinX;
        public int MaxX;

        public int MinY;
        public int MaxY;

        private Texture2D Map;

        public Thread Worker;

        public CoreTask(int x1, int x2, int y1, int y2, Texture2D map) {
            MinX = x1;
            MaxX = x2;
            MinY = y1;
            MaxY = y2;
            Map = map;
            
        }

        public void Perform() {

            int x = MinX+1;
            int y = MinY+1;
            while (x < MaxX)
            {
                while (y < MaxY)
                {
                    Automaton.Cells[x, y].Update();
                    //Map.SetPixel(x, y,Automaton.PixelRenderer.RenderPixel(Automaton.Cells[x, y]));
                    y++;
                }
                y = MinY + 1;
                x++;
            }
        }

        public void Prepare() {
            Worker = new Thread(Perform);
        }

        public void StartWorking() {
            Worker.Start();
        }


    }
}
