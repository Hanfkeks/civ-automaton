﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class Faction
    {
        public const double TECH_LEVEL_COST = 650;
        public const double TECH_UPGRADE_MODIFIER = 2.5;

        public Faction(int nr) {
            FactionNr = nr;
        }

        public int FactionNr;
        
        public int Births=0;
        public int Starvations=0;
        public int AgeDeaths=0;
        public int CombatDeaths=0;

        public int ControlledFields = 0;
        public int Population = 0;
        public double TotalStrength = 0;
        public int StructureCount = 0;
        public int InfectedPopulation = 0;

        public double Science;
        public double NextTechCost = TECH_LEVEL_COST;
        public int TechLevel = 1;

        public string TechString
        {
            get
            {
                switch (TechLevel)
                {
                    case 1:
                        return "Scientific Method";
                    case 2:
                        return "Religion";
                    case 3:
                        return "Warfare";
                    case 4:
                        return "Medicine";
                    case 5:
                        return "Advanced Warfare";
                    case 6:
                        return "Nuclear Power";
                    case 7:
                        return "Biological Weapons";
                    case 8:
                        return "Laser Rays";
                    case 9:
                        return "Mind Control";
                    case 10:
                        return "Heavy Artillery";
                    case 11:
                        return "Laser Satelites";
                    case 12:
                        return "Gene Clinics";
                    default:
                        return "Advanced Lasers (" + (TechLevel - 11)+")";
                }
            }
        }

        public void ResetStats()
        {
            ControlledFields = 0;
            Population = 0;
            TotalStrength = 0;
            StructureCount = 0;
            InfectedPopulation = 0;
        }

        public void AddScience(double science) {
            lock (this) {
                Science += science;
                if (Science > NextTechCost) {
                    Science -= NextTechCost;
                    NextTechCost *= TECH_UPGRADE_MODIFIER;
                    TechLevel++;
                    switch (TechLevel) { 
                        case 2:
                            Debug.Log("Player " + FactionNr + " researched Religion (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 3:
                            Debug.Log("Player " + FactionNr + " researched Warfare (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 4:
                            Debug.Log("Player " + FactionNr + " researched Medicine (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 5:
                            Debug.Log("Player " + FactionNr + " researched Special Forces (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 6:
                            Debug.Log("Player " + FactionNr + " researched Nuclear Weapons (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 7:
                            Debug.Log("Player " + FactionNr + " researched Bio Weapons (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 8:
                            Debug.Log("Player " + FactionNr + " researched Laser Rays (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 9:
                            Debug.Log("Player " + FactionNr + " researched Mind Control (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 10:
                            Debug.Log("Player " + FactionNr + " researched Terraforming (Tech " + (TechLevel - 1) + ")");
                            break;
                        case 11:
                            Debug.Log("Player " + FactionNr + " researched Laser Satellites (Tech " + (TechLevel - 1) + ")");
                            break;
                        default:
                            Debug.Log("Player "+FactionNr+" researched better Lasers (Tech "+(TechLevel-1)+")");
                            break;
                    }
                }
            }
        }

        public void DebugFaction() {
            string dbstring = "Faction: " + FactionNr + "\nBirths: " + Births + "\nStarvations: " + Starvations + "\nNatural Deaths: " + AgeDeaths;
            Debug.Log(dbstring);
        }
    }
}
