﻿using UnityEngine;
public class CamMover : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        ApplyZoom();
    }

    private float dynamicZoom;
    public float zoomPerSecond;
    public float Zoom;
    public float MinZoom;
    public float MaxZoom;
    public float Speed = 0.01f;
    public static Vector3 LastMouse;
    public static Vector3 DeltaMouse;
    public float DoubleClickDelay = 0.4f;

    public Vector2 FocusOffset;
    public float LastClick;

    public Camera Cam;

    // Update is called once per frame
    void Update()
    {
        Vector2 MouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        DeltaMouse = LastMouse - Input.mousePosition;
        LastMouse = Input.mousePosition;

        

        if (Input.GetMouseButton(1))
        {
            
            transform.position += DeltaMouse * Speed * Cam.orthographicSize;
            
        }
        if (Input.GetMouseButton(0))
        {

            Collider2D collider = Physics2D.OverlapPoint(MouseWorldPos);

            
        }
        if (Input.mouseScrollDelta.y != 0)
        {
            dynamicZoom += Input.mouseScrollDelta.y / 4.0f; //Change to Zoom Sensitivity
            zoomPerSecond = (Zoom - dynamicZoom) / 1.0f;  //Change to Zoomspeed
        }
        if (zoomPerSecond != 0)
        {
            if (Mathf.Abs(dynamicZoom) > Time.deltaTime * zoomPerSecond)
            {
                Zoom += dynamicZoom;
                dynamicZoom = 0;
                zoomPerSecond = 0;
            }
            else
            {
                Zoom += Time.deltaTime * zoomPerSecond;
                dynamicZoom -= Time.deltaTime * zoomPerSecond;
            }
        }
        ApplyZoom();
    }

    public void ApplyZoom()
    {
        if (Zoom < MinZoom)
        {
            Zoom = MinZoom;
            dynamicZoom = 0;
            zoomPerSecond = 0;
        }
        if (Zoom > MaxZoom)
        {
            Zoom = MaxZoom;
            dynamicZoom = 0;
            zoomPerSecond = 0;
        }
        Cam.orthographicSize = Mathf.Pow(1.5f, Zoom);
    }

}
