﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class ArmyBase:Superstructure
    {


        public ArmyBase(int x, int y, int f)
            : base(x, y, f)
        { 
            
        }

        public override void Tick()
        {
            base.Tick();
            foreach (Person bob in Automaton.Cells[X, Y].People) {
                if (bob.Profession == Profession.Warrior ||bob.Profession == Profession.Commando) {
                    bob.BonusStrength += (bob.Strength*Effectivity)/5.0f;                    
                    return;
                }
            }
        }

    }
}
