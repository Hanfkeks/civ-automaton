﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class Pentagon:Superstructure
    {

        int RemainingRecruits = 0;

        public Pentagon(int x, int y, int f)
            : base(x, y, f)
        { 
            
        }

        public override void Tick()
        {
            base.Tick();
            if (RemainingRecruits > 0) { 
                foreach (Person bob in Automaton.Cells[X, Y].People) {
                    if (bob.Profession == Profession.Nothing) {
                        bob.Profession = Profession.Commando;
                        bob.BonusStrength = (bob.Strength*10);
                        RemainingRecruits--;
                        return;
                    }
                }
            }
        }

        public override void Upgrade()
        {
            RemainingRecruits++;
        }

    }
}
