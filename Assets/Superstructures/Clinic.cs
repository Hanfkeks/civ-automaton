﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class Clinic:Superstructure
    {

        public const float RANGE_PER_EFFECTIVENESS = 5;
        public const float AOE_PER_EFFECTIVENESS = 4.5f;

        public Clinic(int x, int y, int f) : base(x, y, f)
        { 
            
        }

        public override void Tick()
        {
            base.Tick();
            //pick field
            
        }

        public override void Upgrade()
        {
            base.Upgrade();
            int range = (int)(RANGE_PER_EFFECTIVENESS * Effectivity);
            int drx = Automaton.GameRand.Next(-range, range + 1);
            int dry = Automaton.GameRand.Next(-range, range + 1);
            Cell c = Automaton.GetPropperCell(drx + X, dry + Y);
            if (c != null)
            {
                bool hasInfected = false;
                foreach (Person p in c.People)
                {
                    hasInfected |= p.Infected;
                    break;
                }
                if (!hasInfected)
                {
                    foreach (Person p in c.JoiningPeople)
                    {
                        hasInfected |= p.Infected;
                        break;
                    }
                }
                if (hasInfected && c.Faction == Faction)
                {
                    Automaton.QueueExplosion(c.X, c.Y, (int)(AOE_PER_EFFECTIVENESS * Effectivity), ExplosionType.Heal);
                }
            }
        }

    }
}
