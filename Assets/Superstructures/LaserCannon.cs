﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class LaserCannon:Superstructure
    {
        public const float RANGE_PER_EFFECTIVENESS = 20;
        public const float AOE_PER_EFFECTIVENESS = 0.5f;


        public LaserCannon(int x, int y, int f)
            : base(x, y, f)
        {
            Debug.Log("Laser constructed");
        }

        public override void Upgrade()
        {
            //pick degrees
            Debug.Log("Laser Upgraded");
            int range = (int)(40+RANGE_PER_EFFECTIVENESS*Effectivity);
            int thickness = 1 + (int)(AOE_PER_EFFECTIVENESS * Effectivity);
            float randDeg = (float)(Automaton.GameRand.NextDouble()*360);
            Vector2 dv =  (Vector2)(Quaternion.Euler(0,0,randDeg) * Vector2.right);
            Vector2 totalDv = Vector2.zero;
            Vector2 lastDv = Vector2.zero;
            while(totalDv.magnitude<range){
                int its = 0;
                while ((int)totalDv.x == (int)lastDv.x && (int)totalDv.y == (int)lastDv.y && its < 5) {
                    totalDv += dv;
                    its++;
                }
                lastDv = totalDv;
                Cell c = Automaton.GetPropperCell((int)totalDv.x+X,(int)totalDv.y+Y);
                if (c != null)
                {
                    Automaton.QueueExplosion(c.X, c.Y, thickness, ExplosionType.RectangularLaser, Faction);
                       
                }
                else {
                    break;
                }
            }
            //Debug.Log("Laser Fired!");
            
            base.Upgrade();

            return;
        }
    }
}
