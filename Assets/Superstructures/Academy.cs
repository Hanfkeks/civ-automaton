﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public class Academy:Superstructure
    {


        public Academy(int x, int y, int f)
            : base(x, y, f)
        { 
            
        }

        public override void Tick()
        {
            base.Tick();
            Automaton.Factions[Faction].AddScience((double)Effectivity);
        }

    }
}
