﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class LaserSatellite:Superstructure
    {
        public const float AOE_PER_EXTRA_LEVEL = 1;
        public const float AOE_PER_EFFECTIVENESS = 0.4f;

        public int SatX;
        public int SatY;
        public int LastMovement = -1;

        public LaserSatellite(int x, int y, int f)
            : base(x, y, f)
        {
            SatX = x;
            SatY = y;
        }

        public override void Upgrade()
        {
            Automaton.QueueExplosion(SatX, SatY, (int)(3*(AOE_PER_EXTRA_LEVEL * Automaton.Factions[Faction].TechLevel - 9)), ExplosionType.Laser, Faction);
        }

        public override void Tick()
        {
            //Move Satellite
            int movement = LastMovement;
            if (movement == -1 || Automaton.GameRand.Next(0, 20)==1)
            {
                movement = Automaton.GameRand.Next(0, 4);
            }
            int nx=SatX;
            int ny=SatY;
            
            switch (movement) { 
                case 0:
                    nx = SatX + 1;
                    ny = SatY;
                    break;
                case 1:
                    nx = SatX - 1;
                    ny = SatY;
                    break;
                case 2:
                    nx = SatX;
                    ny = SatY+1;
                    break;
                case 3:
                    nx = SatX;
                    ny = SatY - 1;
                    break;
            }
            LastMovement = movement;
            Cell c = Automaton.GetPropperCell(nx,ny);
            if (c != null)
            {
                SatX = c.X;
                SatY = c.Y;
            }
            else {
                LastMovement = -1;
            }
            Automaton.QueueExplosion(SatX, SatY, (int)(1+(AOE_PER_EXTRA_LEVEL*Automaton.Factions[Faction].TechLevel-8)), ExplosionType.Laser, Faction);
            return;
        }

    }
}
