﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class Silo:Superstructure
    {
        public const float RANGE_PER_EFFECTIVENESS = 22;
        public const float AOE_PER_EFFECTIVENESS = 5;


        public Silo(int x, int y, int f) : base(x, y, f) { 
            
        }

        public override void Upgrade()
        {
            //pick field
            int range = (int)(RANGE_PER_EFFECTIVENESS*Effectivity);
            int drx = Automaton.GameRand.Next(-range,range+1);
            int dry = Automaton.GameRand.Next(-range, range + 1);
            if (dry + Y < 0 || dry+Y>Automaton.Cells.GetLength(1))
            {
                dry = -dry;
            }
            Cell c = Automaton.GetPropperCell(drx+X,dry+Y);
            if (c != null)
            {
                if (c.Faction != Faction && c.Faction!=0) {
                    int radius = (int)(AOE_PER_EFFECTIVENESS*Effectivity);
                    Automaton.QueueExplosion(c.X, c.Y, radius,ExplosionType.Nuclear );
                    if(radius > 20 && !Cell.AllowWarming)
                    {
                        Debug.Log("Global Warming is starting!");
                        Cell.AllowWarming = true;
                    }
                }
            }
            
            base.Upgrade();
            
            return;
        }

    }
}
