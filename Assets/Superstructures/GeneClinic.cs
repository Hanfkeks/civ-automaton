﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Superstructures
{
    public class GeneClinic:Superstructure
    {
        public const float RANGE_PER_EFFECTIVENESS = 10;
        public const float AOE_PER_EFFECTIVENESS = 4.5f;

        public GeneClinic(int x, int y, int f)
            : base(x, y, f)
        { 
            
        }

        public override void Tick()
        {
            base.Tick();
            Cell c = Automaton.GetPropperCell(X,Y);
            foreach (var person in c.People)
            {
                person.Strength += Effectivity;
            }
            foreach (var person in c.JoiningPeople)
            {
                person.Strength += Effectivity;
            }
        }

        public override void Upgrade()
        {
            base.Upgrade();
            int range = (int)(RANGE_PER_EFFECTIVENESS * Effectivity);
            int drx = Automaton.GameRand.Next(-range, range + 1);
            int dry = Automaton.GameRand.Next(-range, range + 1);
            Cell c = Automaton.GetPropperCell(drx + X, dry + Y);
            if (c != null)
            {
                
                if (c.Faction == Faction)
                {
                    Automaton.QueueExplosion(c.X, c.Y, (int)(AOE_PER_EFFECTIVENESS * Effectivity), ExplosionType.Heal);
                    Automaton.QueueExplosion(c.X, c.Y, (int)(AOE_PER_EFFECTIVENESS * Effectivity), ExplosionType.StrengthDuplication);
                }
            }
        }
    }
}
