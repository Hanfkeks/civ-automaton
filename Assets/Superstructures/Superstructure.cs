﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    public abstract class Superstructure
    {
        public int X;
        public int Y;

        public int Faction;

        public int Level = 1;
        public float Effectivity = 1.0f;

        public virtual void Tick() { }

        public Superstructure(int x, int y, int f)
        {
            X = x;
            Y = y;
            Faction = f;
        }

        public virtual void Upgrade()
        {
            Level++;
            Effectivity =(float) Math.Sqrt(Level);
        }

    }
}
