﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class MindControl:Superstructure
    {
        public const float RANGE_PER_EFFECTIVENESS = 23;
        public const float AOE_PER_EFFECTIVENESS = 4f;


        public MindControl(int x, int y, int f)
            : base(x, y, f)
        { 
            
        }

        public override void Upgrade()
        {
            //pick field
            int range = (int)(RANGE_PER_EFFECTIVENESS*Effectivity);
            int drx = Automaton.GameRand.Next(-range,range+1);
            int dry = Automaton.GameRand.Next(-range, range + 1);
            if (dry + Y < 0 || dry+Y>Automaton.Cells.GetLength(1))
            {
                dry = -dry;
            }
            Cell c = Automaton.GetPropperCell(drx+X,dry+Y);
            if (c != null)
            {
                if (c.Faction != Faction && c.Faction!=0) {
                    Automaton.QueueExplosion(c.X, c.Y, (int)(AOE_PER_EFFECTIVENESS*Effectivity),ExplosionType.Conversion,Faction);
                    Debug.Log("Mind Control Device Fired at "+c.X+" , "+c.Y+"!");
                }
            }
            base.Upgrade();

            return;
        }

    }
}
