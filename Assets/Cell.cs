﻿using Assets.Superstructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Assets
{
    public class Cell
    {
        public const int IMPROVEMENT_COST_FACTOR = 15;
        public const double SUPERSTRUCTURE_LEVEL_COST = 1000;
        public const float BASE_HABITABILITY_REQUIREMENT = 14.0f;
        public const float BASE_FERTILITY_REQUIREMENT = 10.0f;
        public const float INITIAL_MIN_WATER_DEPTH = 45.0f;

        public static uint LastDebug;
        public static float GlobalWarmingModifier = 1f;
        public static bool AllowWarming;

        public bool NeedsUpdate = false;

        public const int MAX_HABITABILITY = 2;
        public const int MIN_HABITABILITY = 2;

        public const int MAX_FERTILITY = 4;
        public const int MIN_FERTILITY = 1;

        public int Fallout = 0;

        public Superstructure Superstructure = null;
        public double ScienceValue = 0;

        public int X;
        public int Y;

        public int NaturalFertility;
        public int NaturalHabitability;

        public int UpgradedFertility = 0;
        public int UpgradedHabitability = 0;

        public int Fertility { get { return UpgradedFertility + NaturalFertility; } }
        public int Habitability { get { return UpgradedHabitability + NaturalHabitability; } }

        public float HabitabilityProgress
        {
            get { return _habitabilityProgress; }
            set
            {
                _habitabilityProgress = value;
                if (value > HabitabilityRequirement) { UpgradeHabitability(); }
            }
        }
        public float FertilityProgress
        {
            get { return _fertilityProgress; }
            set
            {
                _fertilityProgress = value;
                if (value > FertilityRequirement) { UpgradeFertility(); }
            }
        }
        private float _habitabilityProgress = 0.0f;
        private float _fertilityProgress = 0.0f;

        public float HabitabilityRequirement = BASE_HABITABILITY_REQUIREMENT;
        public float FertilityRequirement = BASE_FERTILITY_REQUIREMENT;
        public float WaterDepth = 0f;

        public bool Water = false;
        public List<Person> People = new List<Person>();
        public List<Person> LeavingPeople = new List<Person>();
        public List<Person> JoiningPeople = new List<Person>();

        public int PeopleCount { get { return People.Count - LeavingPeople.Count + JoiningPeople.Count; } }

        public Person StrongestMale
        {
            get
            {
                Person strongest = null;
                foreach (Person p in People)
                {
                    if (LeavingPeople.Contains(p) || p.Sex)
                    {
                        continue;
                    }
                    if (strongest == null || p.Strength > strongest.Strength)
                    {
                        strongest = p;
                    }
                }

                foreach (Person p in JoiningPeople)
                {
                    if (LeavingPeople.Contains(p) || p.Sex)
                    {
                        continue;
                    }
                    if (strongest == null || p.Strength > strongest.Strength)
                    {
                        strongest = p;
                    }
                }

                return strongest;
            }
        }

        public Person AnyPerson
        {
            get
            {
                if (People.Count + JoiningPeople.Count == 0)
                {
                    return null;
                }
                else
                {
                    int rand = Automaton.GameRand.Next(0, (People.Count + JoiningPeople.Count));
                    if (rand >= People.Count)
                    {
                        rand -= People.Count;
                        return JoiningPeople[rand];
                    }
                    return People[rand];
                }
            }
        }


        public Person WeakestLink
        {
            get
            {
                Person weakest = null;
                foreach (Person p in People)
                {
                    if (LeavingPeople.Contains(p))
                    {
                        continue;
                    }
                    if (weakest == null || p.Attack < weakest.Attack)
                    {
                        weakest = p;
                    }
                }

                foreach (Person p in JoiningPeople)
                {
                    if (LeavingPeople.Contains(p))
                    {
                        continue;
                    }
                    if (weakest == null || p.Attack < weakest.Attack)
                    {
                        weakest = p;
                    }
                }

                return weakest;
            }
        }

        public int Faction = 0;

        public bool HasMale
        {
            get
            {
                foreach (Person p in People)
                {
                    if (!p.Sex)
                    {
                        return true;
                    }
                }
                foreach (Person p in JoiningPeople)
                {
                    if (!p.Sex)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public Cell(int x, int y, float r, float g, float b)
        {
            X = x;
            Y = y;
            if (b > r && b > g && b> (0))
            {
                Water = true;
                WaterDepth = (b * 255 * Cell.INITIAL_MIN_WATER_DEPTH) + Cell.INITIAL_MIN_WATER_DEPTH* ( (float)Automaton.GameRand.NextDouble() + 0.5f);
                
            }
            NaturalFertility = (int)(MIN_FERTILITY + (g * MAX_FERTILITY));
            NaturalHabitability = (int)(MIN_HABITABILITY + (r * MAX_HABITABILITY));
        }

        public void UpgradeHabitability()
        {
            _habitabilityProgress -= HabitabilityRequirement;
            HabitabilityRequirement *= IMPROVEMENT_COST_FACTOR;
            UpgradedHabitability++;
        }

        public void UpgradeFertility()
        {
            _fertilityProgress -= FertilityRequirement;
            FertilityRequirement *= IMPROVEMENT_COST_FACTOR;
            UpgradedFertility++;
        }

        public void DowngradeHabitability()
        {
            _habitabilityProgress = 0;
            if (UpgradedHabitability == 0)
            {
                return;
            }

            HabitabilityRequirement /= IMPROVEMENT_COST_FACTOR;
            UpgradedHabitability = Math.Max(0, UpgradedHabitability - 1);
        }

        public void AttemptSwap(Person p)
        {
            if (Automaton.GameRand.Next(0, 30) != 0)
            {
                return;
            }
            if (this.People.Count > this.LeavingPeople.Count)
            {
                //Random person is being displaced
                int rand = Automaton.GameRand.Next(0, this.People.Count);
                Person displaced = this.People[rand];
                //if person is already leaving, swap is not happening!
                if (this.LeavingPeople.Contains(displaced))
                {
                    return;
                }
                
                Cell oldcel = Automaton.Cells[p.X, p.Y];
                if (oldcel.Water)
                { return; }
                oldcel.Leave(p);
                oldcel.Enter(displaced);
                this.Leave(displaced);
                this.Enter(p);
            }
        }

        public void DowngradeFertility()
        {
            _fertilityProgress = 0;
            if (UpgradedHabitability == 0)
            {
                return;
            }

            FertilityRequirement /= IMPROVEMENT_COST_FACTOR;
            UpgradedFertility = Math.Max(0, UpgradedFertility - 1);
        }

        public void Update()
        {
            if (!NeedsUpdate)
            {
                return;
            }
            if (Faction != 0)
            {
                lock (Automaton.Factions[Faction])
                { 
                    Automaton.Factions[Faction].ControlledFields++;
                    if (Superstructure != null)
                    {
                        Automaton.Factions[Faction].StructureCount++;
                    }
                }
            }
            Fallout--;
            Fallout = Math.Max(0,Fallout);
            foreach (Person p in JoiningPeople)
            {
                People.Add(p);
            }
            foreach (Person p in LeavingPeople)
            {
                People.Remove(p);
            }
            JoiningPeople.Clear();
            LeavingPeople.Clear();

            float foodPerPerson = ((float)Fertility) / PeopleCount;
            foreach (Person p in People)
            {
                p.Fatigue = Math.Max(p.Fatigue + foodPerPerson, p.MaxFatigue);

                p.Update();
            }
            if (Superstructure != null)
            {
                Superstructure.Tick();
            }

            if (ScienceValue > SUPERSTRUCTURE_LEVEL_COST)
            {
                ScienceValue -= SUPERSTRUCTURE_LEVEL_COST;
                if (Superstructure == null)
                {
                    switch (Automaton.GameRand.Next(0, Math.Min(12,Automaton.Factions[Faction].TechLevel)))
                    {
                        case 0:
                            //Academy
                            //Produces Science for this Faction
                            Superstructure = new Academy(X, Y, Faction);
                            break;
                        case 1:
                            // Cathedral
                            // Creates Monks that can convert stacks of hostile units on a field, can also go on Water
                            Superstructure = new Cathedral(X, Y, Faction);
                            break;
                        case 2:
                            // Army Base
                            // Adds bonus damage to Warriors
                            Superstructure = new ArmyBase(X, Y, Faction);
                            break;
                        case 3:
                            // Clinic
                            // Cures People in a specific range
                            Superstructure = new Clinic(X, Y, Faction);
                            break;
                        case 4:
                            // Pentagon
                            // Produces Special Forces with 10x strength that cut through enemy lines
                            Superstructure = new Pentagon(X, Y, Faction);
                            break;
                        case 5:
                            // Nuclear Silo
                            // Nukes shit and causes fallout
                            Superstructure = new Silo(X, Y, Faction);
                            break;
                        case 6:
                            //Bio Launcher
                            //Causes Infection and reverts immunity in a specific radius
                            Superstructure = new BioLauncher(X, Y, Faction);
                            break;
                        case 7:
                            //Laser Cannon
                            //Kills hostiles in a straight line
                            Superstructure = new LaserCannon(X,Y,Faction);
                            break;
                        case 8:
                            //Brain Wave Emitter
                            //Converts People
                            Superstructure = new MindControl(X, Y, Faction);
                            break;
                        case 9:
                            //Turns people without a profession into Artillery
                            //Artillery randomly bombs hostile areas
                            Superstructure = new ArtilleryBase(X, Y, Faction);
                            break;
                        case 10:
                            //Laser Satellite Controller
                            //Targets a random area
                            //Fries area if area is enemy once per tick
                            //Moves around randomly once per tick
                            //Blast Radius = Additional Tech level
                            //Structure Upgrade causes 
                            Superstructure = new LaserSatellite(X, Y, Faction);
                            break;
                        case 11:
                            //Upgrades strength permanently
                            Superstructure = new GeneClinic(X,Y,Faction);
                            break;
                        case 12:
                            //Terraformer
                            //Turns sea into Land
                            Superstructure = new Terraformer(X, Y, Faction);
                            break;
                    }
                }
                else
                {
                    Superstructure.Upgrade();
                }
            }

            foreach (Person p in LeavingPeople)
            {
                People.Remove(p);
                JoiningPeople.Remove(p);
            }
            foreach (Person p in JoiningPeople)
            {
                People.Add(p);
            }

            if (AllowWarming && Water)
            {
                //Debug.Log("Calculating Global Warming");
                WaterDepth -= GlobalWarmingModifier;
                if (WaterDepth < 0)
                {
                    Water = false;
                    Debug.Log("Global Warming at "+X+" ; "+Y);
                }
            }

            JoiningPeople.Clear();
            LeavingPeople.Clear();

            if (People.Count == 0)
            {
                Faction = 0;
            }
            else
            {
                Faction = People.First().Faction;
            }
            if (People.Count == 0 && Fallout==0)
            {
                NeedsUpdate = false;
            }
        }

        public void Annihilate()
        {
            JoiningPeople.Clear();
            People.Clear();
            ScienceValue = 0;
            Superstructure = null;
            UpgradedFertility = 0;
            UpgradedHabitability = 0;
            _habitabilityProgress = 0;
            _fertilityProgress = 0;
            FertilityRequirement = BASE_FERTILITY_REQUIREMENT;
            HabitabilityRequirement = BASE_HABITABILITY_REQUIREMENT;
        }

        public void Enter(Person p)
        {
            while (p.Faction != this.Faction)
            {
                Person bufWeak = WeakestLink;
                if (bufWeak != null)
                {
                    if (p.Profession == Profession.Monk)
                    {
                        this.Faction = p.Faction;
                        if (this.Habitability <= this.PeopleCount) {
                            Leave(bufWeak);//Purge the weakest link so there's space for the monk
                        }
                        foreach (Person per in People) {
                            per.Faction = p.Faction;
                        }
                        foreach (Person per in JoiningPeople)
                        {
                            per.Faction = p.Faction;
                        }
                    }
                    else
                    {
                        if (bufWeak.Attack > p.Attack)
                        {
                            NeedsUpdate = true;
                            return;
                        }
                        else
                        {
                            LeavingPeople.Add(bufWeak);
                            int rnd = Automaton.GameRand.Next(0, 6);
                            if (rnd == 1)
                            {
                                DowngradeFertility();
                            }
                            else if (rnd < 3)
                            {
                                DowngradeHabitability();
                            }
                        }
                    }
                }
                else
                {
                    this.Faction = p.Faction;
                    this.Superstructure = null;
                    this.ScienceValue = 0;
                }
            }
            JoiningPeople.Add(p);
            p.X = X;
            p.Y = Y;
            NeedsUpdate = true;
        }

        public void Leave(Person p)
        {
            LeavingPeople.Add(p);
            NeedsUpdate = true;
        }

        public int CanEnter(Person p)
        {
            if (Fallout != 0) { return 0; }
            if (Water && p.Profession != Profession.Traveler && p.Profession != Profession.Monk) { return 0; }
            if (p.Faction != this.Faction)
            {
                return 1;
            }
            if (PeopleCount >= Habitability)
            {
                return -1;
            }
            return 1;
        }

    }
}
