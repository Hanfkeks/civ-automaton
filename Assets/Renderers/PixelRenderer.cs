﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public abstract class PixelRenderer
    {
        public abstract Color RenderPixel(Cell c);
    }
}
