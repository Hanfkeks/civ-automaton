﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class PopulationRenderer:PixelRenderer
    {

        public const int DrawsTillDecrease = 2000000;

        public int MaxPopulation=0;

        public int LastMaxPop = 0;

        public override Color RenderPixel(Cell c) {

            LastMaxPop++;
            int pop = c.PeopleCount;
            if (pop >= MaxPopulation)
            {
                MaxPopulation = pop;
                LastMaxPop = 0;
            }
            if (LastMaxPop > DrawsTillDecrease)
            {
                LastMaxPop = DrawsTillDecrease;
                MaxPopulation--;
            }
            float b = pop / ((float)MaxPopulation);

            return new Color(b/2,b/2,b);

        }
    }
}
