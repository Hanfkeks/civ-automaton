﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class FalloutRenderer:PixelRenderer
    {

        public const int DrawsTillDecrease = 600*300*3;

        public int MaxFallout=0;

        public int LastMaxFallout = 0;

        public override Color RenderPixel(Cell c) {
            float b = 0;
            if (c.Water) {
                b = 1;
            }
            
            int pop = c.Fallout;
            if (pop != 0) {
                LastMaxFallout++;
            }
            if (pop >= MaxFallout)
            {
                MaxFallout = pop;
                LastMaxFallout = 0;
            }
            if (LastMaxFallout > DrawsTillDecrease)
            {
                LastMaxFallout = DrawsTillDecrease;
                MaxFallout--;
            }
            float r = pop / ((float)MaxFallout);

            return new Color(r,0,b);

        }
    }
}
