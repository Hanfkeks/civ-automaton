﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class StrengthRenderer : PixelRenderer
    {

        public const int DrawsTillDecrease = 2000000;

        public double MaxStrength = 0;

        public double LastMaxStrength = 0;

        public override Color RenderPixel(Cell c)
        {

            LastMaxStrength++;
            double strength = 0;
            foreach(Person per in c.People){
                strength = Math.Max(strength,per.Strength);
            }
            //strength = Math.Sqrt(strength);
            if (strength >= MaxStrength)
            {
                MaxStrength = strength;
                LastMaxStrength = 0;
            }
            if (LastMaxStrength > DrawsTillDecrease)
            {
                LastMaxStrength = 0;
                MaxStrength--;
            }
            float r =(float)( strength / (MaxStrength));
            if (strength == 0 && c.Water) {
                return new Color(0, 0, 1);
            }

            return new Color(r , r/2, r/4);

        }
    }
}