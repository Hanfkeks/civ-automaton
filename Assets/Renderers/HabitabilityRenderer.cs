﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class HabitabilityRenderer:PixelRenderer
    {

        public const int DrawsTillDecrease = 200000;

        public int MaxHabitability;
        public int MaxFertility;

        public int LastMaxHab = 0;
        public int LastMaxFert = 0;

        public override Color RenderPixel(Cell c) {
            if (c.Water) {
                return Color.blue;
            }

            LastMaxFert++;
            LastMaxHab++;
            int fert = c.Fertility;
            int hab = c.Habitability;
            if(fert>=MaxFertility){
                MaxFertility = fert;
                LastMaxFert = 0;
            }
            if (hab >= MaxHabitability)
            {
                MaxHabitability = hab;
                LastMaxHab = 0;
            }
            if (LastMaxFert > DrawsTillDecrease) {
                LastMaxFert = DrawsTillDecrease;
                MaxFertility--;
            }
            if (LastMaxHab > DrawsTillDecrease)
            {
                LastMaxHab = DrawsTillDecrease;
                MaxHabitability--;
            }
            float g = fert / ((float)MaxFertility);
            float r = hab / ((float)MaxHabitability);

            return new Color(r,g,0);

        }
    }
}
