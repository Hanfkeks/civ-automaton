﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class FactionRenderer:PixelRenderer
    {
        public static Color[] FactionColors = { Color.black, new Color(1f, 0f, 0f), new Color(1f, 1f, 0f), new Color(1f, 0f, 1f), new Color(0f, 1f, 0f), new Color(0f, 1f, 1f), new Color(0f, 0f, 1f) ,
                                                    new Color(1f, 0.5f, 0f), new Color(0.4f, 0.4f, 0.1f), new Color(0.4f, 0.6f, 1f), new Color(0.1f, 0.5f, 0.1f), new Color(0.1f, 0.6f, 0.6f), new Color(0.7f, 0.25f, 0.3f)
                                              };

        public override Color RenderPixel(Cell c) { 
            if(c.Faction==0){
                if(c.Water){
                    return Color.black;
                }else{
                    return new Color(0.8f,0.8f,0.8f);
                }
            }
            if (c.Faction < FactionColors.Count())
            {
                return FactionColors[c.Faction];
            }
            else {
                return new Color(0.4f, 0.2f, 0.2f);
            }
        }

    }
}
