﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class PlagueRenderer:PixelRenderer
    {

        public override Color RenderPixel(Cell c)
        {
            if (c.PeopleCount == 0)
            {
                if (c.Water)
                {
                    return Color.blue;
                }
                else {
                    return Color.black;
                }
            }
            else {
                int infs = 0;
                foreach (Person p in c.People) {
                    if(p.Infected)
                    {
                        infs++;
                    }
                }
                if (infs > 0)
                {
                    return new Color(1, (float)(c.People.Count-infs) / c.People.Count, 0);
                }
                else 
                {
                    return Color.green;
                }
            }
        }

    }
}
