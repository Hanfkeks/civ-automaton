﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class DoctorRenderer:PixelRenderer
    {

        public override Color RenderPixel(Cell c) {
            if (c.People.Count+c.JoiningPeople.Count == 0)
            {
                
                if(c.Water){
                    return new Color(0.2f, 0.2f, 0.2f);
                }else{
                    return Color.black;
                }
            }
            else {
                bool hasDoc = false;
                bool isInfected = false;
                int immunes = 0;
                foreach (Person p in c.People) {
                    if(p.Infected && !c.LeavingPeople.Contains(p)){
                        isInfected = true;
                    }
                    if (p.Immune && !c.LeavingPeople.Contains(p))
                    {
                        immunes++;
                    }
                    if (p.Profession == Profession.Doctor && !c.LeavingPeople.Contains(p))
                    {
                        hasDoc = true;
                    }
                }
                foreach (Person p in c.JoiningPeople)
                {
                    if (p.Infected)
                    {
                        isInfected = true;
                    }
                    if (p.Immune)
                    {
                        immunes++;
                    }
                    if (p.Profession == Profession.Doctor)
                    {
                        hasDoc = true;
                    }
                }
                float r = 0;
                float b = 0;
                if (hasDoc) { b = 1; }
                if (isInfected) { r = 1; }
                float g = (float)immunes / (c.People.Count+c.JoiningPeople.Count);
                return new Color(r,g,b);
            }
        }

    }
}
