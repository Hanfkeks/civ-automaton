﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class SuperstructureRenderer:PixelRenderer
    {

        public const int DrawsTillDecrease = 200000;

        public int MaxLvl = 0;

        public int LastMaxLvl = 0;

        public override Color RenderPixel(Cell c) {

            if (c.Superstructure == null) {
                return Color.black;
            }

            LastMaxLvl++;
            int lvl = c.Superstructure.Level;
            if (lvl >= MaxLvl)
            {
                MaxLvl = lvl;
                LastMaxLvl = 0;
            }
            if (LastMaxLvl > DrawsTillDecrease)
            {
                LastMaxLvl = DrawsTillDecrease;
                MaxLvl--;
            }
            float intensity = lvl / ((float)MaxLvl);
            if (c.Superstructure is Silo)
            {

                return new Color(0.5f+(0.5f * intensity), 0, 0);
            }
            else if (c.Superstructure is Academy)
            {
                return new Color(0, 0.5f + (0.5f * intensity), 0.5f + (0.5f * intensity));
            }
            else if (c.Superstructure is BioLauncher)
            {
                return new Color(0.25f + (0.25f * intensity), 0.5f + (0.5f * intensity), 0);
            }
            else if (c.Superstructure is MindControl)
            {
                return new Color(0.5f + (0.5f * intensity), 0.25f + (0.25f * intensity), 0);
            }
            else if (c.Superstructure is LaserSatellite)
            {
                return new Color(0.5f + (0.5f * intensity), 0.5f + (0.5f * intensity), 0.5f + (0.5f * intensity));
            }
            else if (c.Superstructure is Clinic)
            {
                return new Color(0.5f + (0.5f * intensity), 0, 0.5f + (0.5f * intensity));
            }
            else if (c.Superstructure is ArmyBase) {
                return new Color(0.5f + (0.5f * intensity), 0.5f + (0.5f * intensity), 0);
            }
            else if (c.Superstructure is Pentagon)
            {
                return new Color(0, 0, 0.5f + (0.5f * intensity));
            }
            else if (c.Superstructure is Cathedral)
            {
                return new Color(0, 0.5f + (0.5f * intensity), 0);
            }
            else if (c.Superstructure is Cathedral)
            {
                return new Color(1f, intensity / 2 + 0.25f, intensity / 2 + 0.25f);
            }
            else
            {
                return new Color(intensity / 2 + 0.5f, intensity / 2 + 0.5f, intensity / 2 + 0.5f);
            }

        }
    }
}
