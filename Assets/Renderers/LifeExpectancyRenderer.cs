﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class LifeExpectancyRenderer : PixelRenderer
    {

        public const int DrawsTillDecrease = 200000;

        public float MaxExpectancy = 0;

        public float LastMaxExpectancy = 0;

        public override Color RenderPixel(Cell c)
        {

            LastMaxExpectancy++;
            float expectancy = 0;
            if (c.People.Count == 0)
            {
                return Color.black;
            }
            foreach(Person per in c.People){
                expectancy = Mathf.Max(expectancy, (float)per.AgeExpectancy);
            }
            if (expectancy >= MaxExpectancy)
            {
                MaxExpectancy = expectancy;
                LastMaxExpectancy = 0;
            }
            if (LastMaxExpectancy > DrawsTillDecrease)
            {
                MaxExpectancy--;
                LastMaxExpectancy = 0;
            }
            float r = expectancy / (MaxExpectancy);

            return new Color(r , 0, r);

        }
    }
}