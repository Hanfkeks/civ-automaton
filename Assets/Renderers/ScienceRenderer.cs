﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets
{
    public class ScienceRenderer:PixelRenderer
    {



        public override Color RenderPixel(Cell c) {

            
            float bg = (float)(c.ScienceValue/Cell.SUPERSTRUCTURE_LEVEL_COST);

            return new Color(0,bg,bg);

        }
    }
}
