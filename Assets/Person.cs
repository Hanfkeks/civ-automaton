﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Assets
{
    public class Person
    {
        public const double INCREASE_PLAGUE_CHANCE = 0.00000000001;
        public const double ADULT_AGE = 20;

        public static double PlagueChance = 0.0;

        public bool Immune = false;
        public bool Infected = false;
        public int Faction;
        public Faction FactionReference
        {
            get
            {
                return Automaton.Factions[Faction];
            }
        }
        public double InfectionSeverity = 5;
        public double Strength = 50;
        public double BonusStrength = 0;
        public double Attack
        {
            get
            {
                double totalStrength = Strength + BonusStrength;
                if (Age < ADULT_AGE)
                {
                    totalStrength *= ((Age / ADULT_AGE)*0.15)+0.85;
                }
                else
                {
                    double overage = Age - ADULT_AGE;
                    totalStrength *= 1 + (overage*0.01);
                }
                return totalStrength;
            }
        }
        public double Age = 1;
        public double AgeExpectancy = Automaton.GameRand.Next(70, 100);
        public uint LastUpdated = Automaton.UpdateID;

        public int X;
        public int Y;

        public Profession Profession = Profession.Nothing;

        public int Social = 0;

        public float Fatigue = 2;
        public float MaxFatigue = 4.0f + (float)Automaton.GameRand.NextDouble() * 3.0f;

        public double ConceptionProgress = 0;
        public double ConceptionRequirement = 1;
        public int Conceptions = 0;
        public double LastPartnersStrength;
        public bool LastPartnerImmune = false;

        public bool Sex; //f = true, m = false

        private int lastSeaMovement = -1;

        public Person(int x, int y, int faction, double age, bool sex)
        {
            //Initial Spawn
            Sex = sex;
            Faction = faction;
            X = x;
            Y = y;
            Age = age;
            Immune = true;
            InfectionSeverity = 0;
            Automaton.Cells[X, Y].Enter(this);
        }

        public Person(Person parent)
        {
            Faction = parent.Faction;
            X = parent.X;
            Y = parent.Y;
            Age = 1;
            Strength = (float)(((Automaton.GameRand.NextDouble() * 0.25) + 0.88) * ((parent.Strength+parent.LastPartnersStrength)/2));
            Automaton.Cells[X, Y].Enter(this);
            Sex = Automaton.GameRand.Next(0, 2) == 0;

            int immunities = 0;
            if(parent.Immune)
            {
                immunities++;
            }
            if(LastPartnerImmune)
            {
                immunities++;
            }

            if (immunities>0) {
                
                if (Automaton.GameRand.NextDouble()*2.5 < immunities)
                {
                    Immune = true;
                }
                
            }

            if (Automaton.GameRand.NextDouble() > PlagueChance)
            {
                PlagueChance += INCREASE_PLAGUE_CHANCE;
            }
            else {
                PlagueChance = 0;
                Infected = true;
            }
        }

        public void Update()
        {
            if (LastUpdated == Automaton.UpdateID)
            {
                return;
            }
            if (Immune)
            {
                Infected = false;
            }

            LogStats();

            LastUpdated = Automaton.UpdateID;
            Interlocked.Increment(ref Automaton.PeopleUpdates);
            //Food
            Fatigue -= 1;
            if (Fatigue < 0)
            {
                Automaton.Cells[X, Y].Leave(this);
                FactionReference.Starvations++;
                return;
            }
            //Aging
            Age++;
            if (Age > AgeExpectancy)
            {
                Automaton.Cells[X, Y].Leave(this);
                FactionReference.AgeDeaths++;
                return;
            }
            //Reproduction
            if (!Infected) { 
                if (Sex && ADULT_AGE > 18)
                {
                    Person partner = Automaton.Cells[X, Y].StrongestMale;


                    if (partner != null)
                    {
                        ConceptionProgress += 1;
                        LastPartnersStrength = partner.Strength;
                        LastPartnerImmune = partner.Immune;
                    }
                }
                //ConceptionProgress += 0.4;
                if (ConceptionProgress >= ConceptionRequirement && Automaton.Cells[X, Y].Habitability > Automaton.Cells[X, Y].PeopleCount && !Automaton.Cells[X, Y].Water)
                {
                    ConceptionProgress -= ConceptionRequirement;
                    ConceptionRequirement *= 1.5;
                    Conceptions++;
                    new Person(this);//give birth
                    FactionReference.Births++;
                }
            }

            int movementRand = Automaton.GameRand.Next(0, 4);

            //Profession
            Social += Automaton.Cells[X, Y].PeopleCount - 1;
            switch (Profession)
            {
                case Profession.Nothing:
                    if (Social > 75)
                    {
                        switch (Automaton.GameRand.Next(0, 6))
                        {
                            case 0:
                                Profession = Profession.Architekt;
                                break;
                            case 1:
                                Profession = Profession.Farmer;
                                break;
                            case 2:
                                Profession = Profession.Warrior;
                                BonusStrength = Strength;
                                break;
                            case 3:
                                Profession = Profession.Traveler;
                                break;
                            case 4:
                                Profession = Profession.Doctor;
                                break;
                            case 5:
                                Profession = Profession.Scientist;
                                break;
                        }
                    }
                    break;
                case Profession.Architekt:
                    Automaton.Cells[X, Y].HabitabilityProgress += (Social * 0.006f);
                    break;
                case Profession.Doctor:
                    Person p = Automaton.Cells[X, Y].People[Automaton.GameRand.Next(0, Automaton.Cells[X, Y].People.Count)];
                    p.AgeExpectancy += 0.1 + Social * 0.0005;
                    if (p.Infected) { 
                        p.Infected = false;
                        if (true||Automaton.GameRand.Next(0, 2) == 1) {//For testing purposes 100%
                            p.Immune = true;
                        }
                    }
                    if (Automaton.GameRand.Next(0, 150) == 1) { //Breakthrough, only possible in infected territory
                        bool infOnField = false;
                        foreach (Person pers in Automaton.Cells[X, Y].People) {
                            infOnField = pers.Infected; 
                        }
                        if (infOnField) { 
                            foreach (Person pers in Automaton.Cells[X, Y].People) {
                                pers.Immune = true;
                            }
                            Automaton.Cure(X-1,Y);
                            Automaton.Cure(X + 1, Y);
                            Automaton.Cure(X , Y+1);
                            Automaton.Cure(X , Y-1);

                            Automaton.Cure(X - 1, Y+1);
                            Automaton.Cure(X + 1, Y+1);
                            Automaton.Cure(X+1, Y + 1);
                            Automaton.Cure(X+1, Y - 1);

                            Automaton.Cure(X - 1, Y-1);
                            Automaton.Cure(X + 1, Y-1);
                            Automaton.Cure(X-1, Y + 1);
                            Automaton.Cure(X-1, Y - 1);
                        }
                    }
                    break;
                case Profession.Farmer:
                    Automaton.Cells[X, Y].FertilityProgress += (Social * 0.01f);
                    break;
                case Profession.Artillery:
                case Profession.Monk:
                case Profession.Traveler:
                    AgeExpectancy += 0.6;
                    Fatigue += 0.5f;
                    if (Automaton.Cells[X, Y].Water)
                    {
                        if (lastSeaMovement == -1 || Automaton.GameRand.Next(0, 18) == 1)
                        {
                            lastSeaMovement = movementRand;
                        }
                        else
                        {
                            movementRand = lastSeaMovement;
                        }
                    }
                    else {
                        lastSeaMovement = -1;
                    }
                    if (this.Profession == Profession.Artillery && Automaton.GameRand.Next(0, 4) == 1)
                    {
                        int range = 8;
                        int drx = Automaton.GameRand.Next(-range, range + 1);
                        int dry = Automaton.GameRand.Next(-range, range + 1);
                        if (dry + Y < 0 || dry + Y > Automaton.Cells.GetLength(1))
                        {
                            dry = -dry;
                        }
                        Cell target = Automaton.GetPropperCell(drx + X, dry + Y);
                        if (target != null)
                        {
                            if (target.Faction != Faction && target.Faction != 0)
                            {
                                Automaton.QueueExplosion(target.X, target.Y, 5, ExplosionType.Laser);
                            }
                        }
                    }
                    break;
                case Profession.Scientist:
                    Automaton.Cells[X, Y].ScienceValue += 0.5 + Social * 0.02;
                    break;
            }
            //Movement
            int nx;
            int ny;

            if (Infected) {
                int index = Automaton.GameRand.Next(0, Automaton.Cells[X, Y].People.Count);
                Automaton.Cells[X, Y].People[index].Infected = true;
                Automaton.Cells[X, Y].People[index].InfectionSeverity = InfectionSeverity;
                if (!Automaton.Cells[X, Y].Water || Automaton.GameRand.Next(0,20)==0)
                {
                    //Seamen can carry diseases more unaffected, so they carry them further
                    AgeExpectancy -= InfectionSeverity;
                    InfectionSeverity += 0.1f;
                    int rand = Automaton.GameRand.Next(0, 50);
                    if (rand == 1)
                    {
                        //Miracle healing
                        this.Immune = true;
                    }
                    else if (rand < 5)
                    {
                        this.Infected = false;
                    }
                }
                

            }

            int canMove;
            switch (movementRand)
            {
                case 0:
                    nx = X;
                    ny = Y - 1;
                    if (Infected)
                    {
                        Automaton.Infect(nx, ny, InfectionSeverity);
                    }
                    canMove = Automaton.CanMove(this, nx, ny);
                    if (canMove == 1)
                    {
                        Automaton.Cells[X, Y].Leave(this);
                        Automaton.Cells[nx, ny].Enter(this);
                    }
                    else if (canMove == -1)
                    {
                        Automaton.Cells[nx, ny].AttemptSwap(this);
                    }
                    else {
                        lastSeaMovement = -1;
                    }
                    
                    break;
                case 1:
                    nx = X;
                    ny = Y + 1;
                    if (Infected)
                    {
                        Automaton.Infect(nx, ny, InfectionSeverity);
                    }
                    canMove = Automaton.CanMove(this, nx, ny);
                    if (canMove == 1)
                    {
                        Automaton.Cells[X, Y].Leave(this);
                        Automaton.Cells[nx, ny].Enter(this);
                    }
                    else if (canMove == -1)
                    {
                        Automaton.Cells[nx, ny].AttemptSwap(this);
                    }
                    else
                    {
                        lastSeaMovement = -1;
                    }
                    
                    break;
                case 2:
                    nx = (X - 1);
                    if (nx < 0) {
                        nx += Automaton.Cells.GetLength(0);
                    }
                    ny = Y;
                    if (Infected)
                    {
                        Automaton.Infect(nx, ny, InfectionSeverity);
                    }
                    canMove = Automaton.CanMove(this, nx, ny);
                    if (canMove==1)
                    {
                        Automaton.Cells[X, Y].Leave(this);
                        Automaton.Cells[nx, ny].Enter(this);
                    }
                    else if (canMove == -1)
                    {
                        Automaton.Cells[nx, ny].AttemptSwap(this);
                    }
                    else
                    {
                        lastSeaMovement = -1;
                    }
                    
                    break;
                case 3:
                    nx = (X + 1) % (Automaton.Cells.GetLength(0));
                    ny = Y;
                    if (Infected)
                    {
                        Automaton.Infect(nx, ny, InfectionSeverity);
                    }
                    canMove = Automaton.CanMove(this, nx, ny);
                    if (canMove == 1)
                    {
                        Automaton.Cells[X, Y].Leave(this);
                        Automaton.Cells[nx, ny].Enter(this);
                    }
                    else if (canMove == -1)
                    {
                        Automaton.Cells[nx, ny].AttemptSwap(this);
                    }
                    else
                    {
                        lastSeaMovement = -1;
                    }
                    
                    break;
            }
        }


        public void LogStats()
        { 
            lock(FactionReference)
            {
                FactionReference.TotalStrength += Strength;
                if (Infected)
                {
                    FactionReference.InfectedPopulation++;
                }
                FactionReference.Population++;
            }
            
        }


    }

    public enum Profession
    {
        Nothing,
        Architekt,
        Farmer,
        Warrior,
        Traveler,
        Doctor,
        Scientist,
        Monk,
        Commando,
        Artillery,
    }
}
